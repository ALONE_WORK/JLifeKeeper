﻿#ifndef JDATAJSON_H
#define JDATAJSON_H

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QVector>
#include "Header.h"

class JDataJson
{
private:
    QString FileName;
    QJsonObject PasswdObj;
    QJsonObject BookObj;
public:
    JDataJson();
    JDataJson(QString FileName);

    void SetFileName(QString FileName);
    // 添加一个密码信息数据
    void AddPasswdData(int Index, JDPasswdNote pwd);
    // 添加一个书籍信息数据
    void AddBookData(int Index, JDBookData book);
    // 保存所有数据为数据文件
    bool SaveData();
    // 同时读取密码数据和书籍数据
    bool ReadData(QVector<JDPasswdNote> &PwdList, QVector<JDBookData> &BookList);
};

#endif // JDATAJSON_H
