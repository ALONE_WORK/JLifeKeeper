﻿#include "jlog.h"

JLog::JLog(QObject *parent) : QObject(parent)
{
    LogFilePath = QCoreApplication::applicationDirPath();
    CreateLogPath();
}


QString JLog::GetCurrentTime()
{
    return QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
}

bool JLog::CreateLogPath()
{
    LogFilePath = LogFilePath + "/log_" + QDateTime::currentDateTime().toString("yyyy") + "/" +
            QDateTime::currentDateTime().toString("yyyy-MM");
    QDir dir(LogFilePath);
    if(dir.exists()) {
        return true;
    }
    else {
        if(dir.mkpath(LogFilePath))
            return true;
        else
            return false;
    }
}

void JLog::WriteLog(QString LogInfo)
{
    QString LogFile = LogFilePath + "/" + QDate::currentDate().toString("yyyyMMdd") + ".txt";
    QFile file(LogFile);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Append))
        return;
    LogInfo = "[" + GetCurrentTime() + "] " + LogInfo + "\r\n";
    file.write((char*)LogInfo.toStdString().c_str());
    file.close();
}
