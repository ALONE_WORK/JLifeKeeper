﻿#ifndef JLOG_H
#define JLOG_H

#include <QObject>
#include <QFile>
#include <QtCore>
#include <QTime>
#include <QDateTime>
#include <QDir>

class JLog : public QObject
{
    Q_OBJECT
public:
    explicit JLog(QObject *parent = nullptr);

    QString LogFilePath;

    QString GetCurrentTime();
    bool CreateLogPath();
    void WriteLog(QString LogInfo);
signals:

public slots:
};

#endif // JLOG_H
