﻿#include "datasyncthread.h"
#include <QDebug>
#include <windows.h>
#include "jmysql.h"

DataSyncThread::DataSyncThread(QObject *parent) : QObject(parent)
{

}

void DataSyncThread::SyncData(const int UserId)
{
    // 读取本地数据文件中数据
    JDataJson json;
    QVector<JDPasswdNote> PwdList;
    QVector<JDBookData> BookList;
    json.ReadData(PwdList, BookList);
    // 读取数据库中数据
    JMySQL sql;
    QVector<JDPasswdNote> DbPwdList;
    QVector<JDBookData> DbBookList;
    sql.ReadPasswdData(UserId, DbPwdList);
    sql.ReadBookData(UserId, DbBookList);
    // 比较数据
    for(auto i = 0; i < PwdList.size(); i++) {
        JDPasswdNote pwd;
        bool IsExist = false;
        for(auto j = 0; j < DbPwdList.size(); j++) {
            if(PwdList.at(i).Index == DbPwdList.at(j).Index) {
                if(PwdList.at(i).Name != DbPwdList.at(j).Name ||
                        PwdList.at(i).Type != DbPwdList.at(j).Type ||
                        PwdList.at(i).Account != DbPwdList.at(j).Account ||
                        PwdList.at(i).Email != DbPwdList.at(j).Email ||
                        PwdList.at(i).Phone != DbPwdList.at(j).Phone ||
                        PwdList.at(i).Pwd != DbPwdList.at(j).Pwd ||
                        PwdList.at(i).Pwd2 != DbPwdList.at(j).Pwd2 ||
                        PwdList.at(i).Remark != DbPwdList.at(j).Remark) {
                    pwd = PwdList.at(i);
                    pwd.UserId = UserId;
                    sql.UpdatePasswdData(pwd);
                }
                IsExist = true;
                break;
            }
        }
        if(!IsExist) {
            pwd = PwdList.at(i);
            pwd.UserId = UserId;
            sql.WritePasswdData(pwd);
        }
    }
    for(auto i = 0; i < BookList.size(); i++) {
        bool IsExist = false;
        JDBOOK book;
        for(auto j = 0; j < DbBookList.size(); j++) {
            if(BookList.at(i).Index == DbBookList.at(j).Index) {
                if(BookList.at(i).BookName != DbBookList.at(j).BookName ||
                        BookList.at(i).BookType != DbBookList.at(j).BookType ||
                        BookList.at(i).BookAuthor != DbBookList.at(j).BookAuthor ||
                        BookList.at(i).AuthorHome != DbBookList.at(j).AuthorHome ||
                        BookList.at(i).BookPunlish != DbBookList.at(j).BookPunlish ||
                        BookList.at(i).DataTime != DbBookList.at(j).DataTime ||
                        BookList.at(i).BookStatus != DbBookList.at(j).BookStatus ||
                        BookList.at(i).TimeLen != DbBookList.at(j).TimeLen ||
                        BookList.at(i).BookSummary != DbBookList.at(j).BookSummary ||
                        BookList.at(i).BookPlat != DbBookList.at(j).BookPlat ||
                        BookList.at(i).Remark != DbBookList.at(j).Remark) {
                    book = BookList.at(i);
                    book.UserId = UserId;
                    sql.UpdateBookData(book);
                }
                IsExist = true;
                break;
            }
        }
        if(!IsExist) {
            book = BookList.at(i);
            book.UserId = UserId;
            sql.WriteBookData(book);
        }
    }
    emit FinishSync();
}
