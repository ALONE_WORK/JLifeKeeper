﻿#ifndef LIFEKEEPER_H
#define LIFEKEEPER_H

#include <QWidget>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QStatusBar>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QTableWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QTimer>
#include <QDateTime>
#include <string>
#include <deque>
#include <QDialog>
#include <QThread>
#include <QCloseEvent>
#include "Header.h"
#include "jdatajson.h"
#include "datasyncthread.h"
#include "jlog.h"
#include "registerform.h"

namespace Ui {
class LifeKeeper;
}

class LifeKeeper : public QWidget
{
    Q_OBJECT

public:
    explicit LifeKeeper(QWidget *parent = 0);
    ~LifeKeeper();

private:
    Ui::LifeKeeper *ui;
    int TypeMode;
    JDataJson DJson;

private:
    // 控件变量
    QMenuBar *MenuBar;
    QMenu *FileMenu;
    QAction *SaveAction;
    QMenu *ViewMenu;
    QAction *LogInAction;
    QAction *BookAction;
    QAction *PasswdAction;
    QMenu *ToolMenu;
    QToolBar *ToolBar;
    QStatusBar *StatusBar;
    QLabel *LbStatusTipInfo;
    QLineEdit *LEditSearch;
    QPushButton *pBtnSearch;
    QHBoxLayout *HLayPwdSearch;
    QTableWidget *TabWigtPasswd;
    QVBoxLayout *VLayPasswd;
    QTabBar *TabPwd;
    QTabBar *TabBank;
    QTabBar *TabPersonInfo;
    QTabWidget *TabAccount;
    QWidget *WigtBook;
    QVBoxLayout *VLayBook;
    QTableWidget *TabWigtBook;
    QVBoxLayout *VLayMain;
    QHBoxLayout *HLayBookSearch;
    QLineEdit *LEditBookSearch;
    QPushButton *pBtnBookSearch;
    // 右击菜单
    QMenu *MenuPwd;
    QAction *ActionAdd;
    QAction *ActionDel;
    QMenu *MenuBook;
    QAction *ActionAddBook;
    QAction *ActionDelBook;
    // 登录
    QDialog *LogFram;
    QPushButton *pBtnLogSure;
    QPushButton *pBtnLogFail;
    QLabel *LabLogUser;
    QLineEdit *LEditLogUser;
    QLabel *LabLogPasswd;
    QLineEdit *LEditLogPasswd;
    QLabel *LabLogUserTip;
    QLabel *LabLogPasswdTip;
    QGridLayout *GLayLog;
    QCheckBox *CheckBoxRem;
    QPushButton *pBtnRegister;
    QTimer *PwdTimer;
    bool IsLog;
    // 注册
    RegisterForm *Reg;
    // 线程
    QThread *Thread;
    DataSyncThread *SyncThread;
    // 其他
    int UserId;
    JLog log;
    int BookIndex;
    int PwdIndex;
    QVector<JDPasswdNote> PwdDataList;
    QVector<JDBookData> BookDataList;

signals:
    void StartSyncData(const int UserId);

    // 函数
private:
    void Init();
    void InitCtrls();
    void InitPasswd();
    void InitBook();
    void InitVariates();
    // 初始化登录界面
    void InitLogFrame();
    // 初始化线程
    void InitThreads();
    // 获取时间函数, msec = true:返回时间带毫秒,否则不带
    QString GetCurrentTime(bool msec = false);
    // 加载数据
    void LoadDatatoCtrl();
    // 设置密码本数据
    void SetPasswdData(JDPasswdNote Data);
    // 获取密码本控件数据并存储
    void GetAndSavePasswdData();
    // 正则式匹配, Src:原字符串, Dest:匹配字符串
    bool IsMatched(QString Src, QString Dest);
    // 检索密码信息
    void SearchPasswd();
    // 显示或隐藏密码信息
    void DisplayPasswd(bool show = true);
    // 获取密码本控件数据并存储
    void GetAndSaveBookData();
    // 设置书籍数据到控件显示
    void SetBookData(JDBookData Data);
    // 显示或隐藏密码信息
    void DisplayBook(bool show = true);
    // 检索书籍信息
    void SearchBook();
    // 休眠
    void Sleep(unsigned int Sec);
private slots:
    void resizeEvent(QResizeEvent *e);
    // 保存事件
    void SaveDataSlot();
    void PasswdNoteSlot();      // 密码本
    void BookNoteSlot();        // 书籍记录
    // 密码本右击菜单事件
    void PasswdcontextMenuEvent(const QPoint& Pos);
    // 书籍本右击菜单事件
    void BookcontextMenuEvent(const QPoint& Pos);
    // 添加一行密码信息事件
    void AddPasswdRowSlot();
    // 删除密码信息事件
    void DeletePasswdRowsSlot();
    // 添加一行密码信息事件
    void AddBookRowSlot();
    // 删除密码信息事件
    void DeleteBookRowsSlot();
    // 登录
    void LoginSlot();
    // 保存密码表中信息
    void SavePasswdInfoSlot();
    // 密码本搜索按钮事件
    void PasswdSearchSlot();
    // 密码信息检索框改变事件
    void PasswdSearchChangedSlot(QString Text);
    // 书籍搜索事件
    void BookSearchSlot();
    // 书籍搜索文本框改变事件
    void BookSearchChangeSlot(QString Text);
    // 登录成功
    void LogSuccessSlot();
    // 登录失败
    void LogFailedSlot();
    // 完成同步
    void FinishSyncData();
    // 注册按钮槽函数
    void RegisterUser();
    // 注册窗口关闭事件
    void closeRefister();
    // 记住密码复选框
    void RemeberPasswdSlot();
    // 注册成功后登陆窗口登陆接收用户信息
    void RefisterSuccess(const UserInformation User);
};

#endif // LIFEKEEPER_H
