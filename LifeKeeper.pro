#-------------------------------------------------
#
# Project created by QtCreator 2018-12-05T22:26:22
#
#-------------------------------------------------

QT       += core gui sql

RC_ICONS = Icon.ico

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LifeKeeper
TEMPLATE = app


SOURCES += main.cpp\
        lifekeeper.cpp \
    datasyncthread.cpp \
    jlog.cpp \
    registerform.cpp

HEADERS  += lifekeeper.h \
    Header.h \
    datasyncthread.h \
    jlog.h \
    jdatajson.h \
    jmysql.h \
    registerform.h

FORMS    += lifekeeper.ui \
    registerform.ui

INCLUDEPATH += ./
LIBS += ./JMySQL.dll \
        JDataJson.dll

DISTFILES += \
    JDataJson.dll \
    JMySQL.dll

RESOURCES += \
    source.qrc
