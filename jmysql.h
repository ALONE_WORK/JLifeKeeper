﻿#ifndef JMYSQL_H
#define JMYSQL_H

#include <QtSql>
#include <QVector>
#include <deque>
#include "Header.h"


class  JMySQL
{
private:
    DataBaseInfo DbInfo;
    QSqlDatabase db;
private:
    void Init(QString ConnectName = "");
public:
    JMySQL();
    JMySQL(QString ConnectName);
    ~JMySQL();
public:
    // 读取用户信息
    bool QueryLogInfo(const QString Name, UserInformation &UserInfo);
    // 写入用户信息
    bool WriteUserInfo(const UserInformation UserInfo);
    // 验证用户名是否存在
    bool IsUserExist(const QString Name);
    // 读取用户密码数据
    uint ReadPasswdData(const int UserId, QVector<JDPasswdNote> &DataList);
    // 写用户密码数据
    bool WritePasswdData(JDPasswdNote Data);
    // 更新用户密码数据
    bool UpdatePasswdData(JDPasswdNote Data);
    // 删除用户密码数据
    bool DeletePasswdData(int UserId, int Index);
    // 读取用户书籍数据
    uint ReadBookData(const int UserId, QVector<JDBookData> &DataList);
    // 写用户书籍数据
    bool WriteBookData(JDBookData Data);
    // 更新用户书籍数据
    bool UpdateBookData(JDBookData Data);
    // 删除用户书籍数据
    bool DeleteBookData(int UserId, int Index);
    // 读取Index值
    bool ReadIndexValue(const int UserId, int Type, int &MaxIndex);
};

#endif // JMYSQL_H
