﻿#include "registerform.h"
#include "ui_registerform.h"
#include "jmysql.h"
#include <QThread>

RegisterForm::RegisterForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RegisterForm)
{
    ui->setupUi(this);
    Init();
}

RegisterForm::~RegisterForm()
{
    delete ui;
}

void RegisterForm::Init()
{
    this->setLayout(ui->vLayoutRM);
}

void RegisterForm::closeEvent(QCloseEvent *e)
{
    emit closeWind();
}

void RegisterForm::on_BtnSure_clicked()
{
    if(UserInfo.User.isEmpty()) {
        ui->LabTipInfo->setText("用户名为空！");
        return;
    }
    if(UserInfo.Passwd.isEmpty()) {
        ui->LabTipInfo->setText("密码为空！");
        return;
    }
    JMySQL sql;
    if(sql.WriteUserInfo(UserInfo)) {
        emit RegisterSuccess(UserInfo);
        qDebug() << "注册成功！";
        this->close();
    }
    else {
        qDebug() << "注册失败！";
        ui->LabTipInfo->setText("注册失败！");
        return;
    }
}

void RegisterForm::on_EditNickName_editingFinished()
{
    QString Name = ui->EditNickName->text();
    if(Name.isEmpty())
        return;
    JMySQL sql;
    if(sql.IsUserExist(Name)) {
        ui->LabTipInfo->setText("用户名已存在！");
        return;
    }
    UserInfo.User = Name;
}

void RegisterForm::on_EditPasswd_editingFinished()
{
    UserInfo.Passwd = ui->EditPasswd->text();
}

void RegisterForm::on_EditSPasswd_editingFinished()
{
    QString Pwd = ui->EditPasswd->text();
    QString Pwd2 = ui->EditSPasswd->text();
    if(Pwd != Pwd) {
        ui->LabTipInfo->setText("密码不一致！");
        return;
    }
    UserInfo.Passwd = ui->EditPasswd->text();
}

void RegisterForm::on_EditReadName_editingFinished()
{
    UserInfo.RealName = ui->EditReadName->text();
}

void RegisterForm::on_EditBirth_editingFinished()
{
    UserInfo.Birth = ui->EditBirth->text();
}

void RegisterForm::on_EditEmail_editingFinished()
{
    UserInfo.Mail = ui->EditEmail->text();
}

void RegisterForm::on_EditPhone_editingFinished()
{
    UserInfo.PhoneNumber = ui->EditPhone->text();
}

void RegisterForm::on_EditWeChat_editingFinished()
{
    UserInfo.WeChat = ui->EditWeChat->text();
}

void RegisterForm::on_EditQQ_editingFinished()
{
    UserInfo.QQ = ui->EditQQ->text();
}

void RegisterForm::on_EditWeb_editingFinished()
{
    UserInfo.PersonWeb = ui->EditWeb->text();
}
