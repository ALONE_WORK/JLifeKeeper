﻿#ifndef HEADER_H
#define HEADER_H
#include <QString>

enum Mode{
    BOOKMODE = 1,
    PASSWDMODE
};

struct DataBaseInfo
{
    QString Server;
    QString User;
    QString Passwd;
    QString DbName;
    DataBaseInfo() {
        Server = "";
        User = "";
        Passwd = "";
        DbName = "";
    }
};

struct UserInformation
{
    int Id;
    QString User;
    QString Passwd;
    QString Mail;
    QString RealName;
    QString Birth;
    int Age;
    int Level;
    QString PhoneNumber;
    QString QQ;
    QString WeChat;
    QString PersonWeb;
    UserInformation() {
        User = "";
        Passwd = "";
        Mail = "";
        RealName = "";
        Birth = "";
        Age = 0;
        Level = 1;
        PhoneNumber = "";
        QQ = "";
        WeChat = "";
        PersonWeb = "";
    }
};

typedef struct JDPasswdNote {
    int Index;              // Id
    int UserId;             // 用户Id
    QString Name;           // 账号产品名称
    QString Type;           // 类型
    QString Account;        // 账号名
    QString Email;          // 邮箱
    QString Phone;          // 电话
    QString Pwd;            // 密码
    QString Pwd2;           // 二级密码
    QString Remark;         // 备注
    JDPasswdNote() {
        UserId = 0;
        Index = 0;
        Name = "";
        Type = "";
        Account = "";
        Email = "";
        Phone = "";
        Pwd = "";
        Pwd2 = "";
        Remark = "";
    }
}JDPWD;

typedef struct JDBookData {
    int Index;              // Id
    int UserId;             // 用户Id
    QString BookName;       // 书名
    QString BookType;       // 书籍类型
    QString BookAuthor;     // 作者
    QString AuthorHome;     // 作者国籍
    QString BookPunlish;    // 出版社
    QString DataTime;       // 看书时间
    QString BookStatus;     // 看书状态
    QString TimeLen;        // 阅读时长
    QString BookSummary;    // 书籍梗概
    QString BookPlat;       // 看书平台
    QString Remark;         // 备注
    JDBookData() {
        UserId = 0;
        Index = 0;
        BookName = "";
        BookType = "";
        BookAuthor = "";
        AuthorHome = "";
        BookPunlish = "";
        DataTime = "";
        BookStatus = "";
        TimeLen = "";
        BookSummary = "";
        BookPlat = "";
        Remark = "";
    }
}JDBOOK;


#endif // HEADER_H
