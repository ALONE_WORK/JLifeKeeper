﻿#ifndef DATASYNCTHREAD_H
#define DATASYNCTHREAD_H

#include <QObject>
#include <deque>
#include "jdatajson.h"
#include "jlog.h"

class DataSyncThread : public QObject
{
    Q_OBJECT
public:
    explicit DataSyncThread(QObject *parent = nullptr);
private:
    JLog log;
signals:
    void FinishSync();

public slots:
    void SyncData(const int UserId);
};

#endif // DATASYNCTHREAD_H
