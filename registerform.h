﻿#ifndef REGISTERFORM_H
#define REGISTERFORM_H

#include <QWidget>
#include <QCloseEvent>
#include "Header.h"

namespace Ui {
class RegisterForm;
}

class RegisterForm : public QWidget
{
    Q_OBJECT

public:
    explicit RegisterForm(QWidget *parent = 0);
    ~RegisterForm();
signals:
    void closeWind();
    void RegisterSuccess(const UserInformation Info);
private:
    Ui::RegisterForm *ui;
    UserInformation UserInfo;
    void Init();
private slots:
    // 窗口关闭事件
    void closeEvent(QCloseEvent *e);
    void on_BtnSure_clicked();
    void on_EditNickName_editingFinished();
    void on_EditPasswd_editingFinished();
    void on_EditSPasswd_editingFinished();
    void on_EditReadName_editingFinished();
    void on_EditBirth_editingFinished();
    void on_EditEmail_editingFinished();
    void on_EditPhone_editingFinished();
    void on_EditWeChat_editingFinished();
    void on_EditQQ_editingFinished();
    void on_EditWeb_editingFinished();
};

#endif // REGISTERFORM_H
